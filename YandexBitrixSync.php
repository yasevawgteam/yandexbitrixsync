<?

class YandexBitrixSync
{
	private $token = 'токен';
	private $iBlockIdRec;
	private $iBlockIdShop;
	private $smallId;
	private $bigId;
	private $imageFromDB = array();
	
	private $folder = '';
	private $serverDiskFolder = 'папка с файлами в корне';
	private $siteName = 'http://домен.ru';
	
	private $smallBanners = array(
		"code" => "small",
		"touch" => ""
	);
	private $bigBanners = array(
		"code" => "big",
		"touch" => ""
	);
	private $jsFile = array(
		'name' => 'js/videojs-syncPlayList.js', 
		'touch' => ''
	);
	private $jsonFile = array(
		'name' => 'list.json', 
		'touch' => ''
	);
	
	public function __construct ($folder, $iBlockIdRec, $iBlockIdShop, $smallId, $bigId) 
	{
		$this->folder = $folder;
		$this->smallBanners['touch'] = $folder;
		$this->bigBanners['touch'] = $folder;
		$this->jsFile['touch'] = $folder;
		$this->jsonFile['touch'] = $folder;
		
		$this->iBlockIdRec = $iBlockIdRec;
		$this->iBlockIdShop = $iBlockIdShop;
		$this->smallId = $smallId;
		$this->bigId = $bigId;	
	}

	private function apiRequest ($yandexPath, $method, $responseCheck = false) 
	{
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_PORT => "443",
			CURLOPT_URL => $yandexPath,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $method,
			CURLOPT_HTTPHEADER => array(
				"authorization: OAuth ".$this->token,
				"cache-control: no-cache"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);

		if($err){
			return false;
		}

		return ($responseCheck) ? $response : true;
	}
	
	/**
	*	Загрузка/удаление файла
	*/
	private function upload ($newPath, $method, $file = '') 
	{
		if ($method == "POST") {
			$yandexPath = "https://cloud-api.yandex.net/v1/disk/resources/upload?path=".$newPath."&url=".$file;
		} elseif ($method == "DELETE") {
			$yandexPath = "https://cloud-api.yandex.net/v1/disk/resources?path=".$newPath;
		} else {
			return false;
		}
		
		return ($this->apiRequest($yandexPath, $method)) ? true : false;
	}
	
	/**
	*	Загрузка/перезапись файла
	*/
	public function uploadOverwrite ($newPath, $method, $file = '') 
	{
		$yandexPath = 'https://cloud-api.yandex.net/v1/disk/resources/upload?path='.$newPath.'&overwrite=true';

		$response = $this->apiRequest($yandexPath, $method, true);

		if (!$response) {
			return false;
		} else {
			
			$responseData = json_decode($response, true);
			$fp = fopen($file, 'r');
			
			$ch = curl_init();
			curl_setopt_array($ch, array(
				CURLOPT_PORT => "443",
				CURLOPT_URL => $responseData['href'],
				CURLOPT_UPLOAD => true,
				CURLOPT_INFILESIZE => filesize($file),
				CURLOPT_INFILE => $fp,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => $responseData['method'],
				CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",
					"content-type: application/x-www-form-urlencoded"
				),
			));

			$err = curl_error($ch);
			curl_close($ch);
			
			return ($err) ? false : true;		
		}		
	}
	
	/**
	*	Выборка всех баннеров
	*/
	private function selectPath ($iBlockIdRec, $elementId, $code) 
	{	
		$res = CIBlockElement::GetProperty($iBlockIdRec, $elementId, "sort", "asc", array("CODE" => $code));

		while ($ob = $res->GetNext())		{
			$values[] = CFile::GetPath($ob['VALUE']);
		}	
		return $values;
	}
	
	/**
	*	Добавление актуальных данных
	**/
	private function addData ()	
	{
		$this->smallBanners['data'] = $this->selectPath($this->iBlockIdRec, $this->smallId, 'BANERS2');
		$this->bigBanners['data'] = $this->selectPath($this->iBlockIdRec, $this->bigId, 'BANERS2');
		$this->getImages();
	}
	
	/**
	*	Получение списка всех баннеров временной папки
	**/
	private function getListLocalFiles ($data) 
	{
		return scandir(
				$_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'. $this->folder.'/banner/'.$data['code'], 
				1
			);
	}
	
	/**
	*	Копирование всех баннеров в папку
	*/
	private function copyBanners ($data) 
	{
		foreach ($data['data'] as $value) {			
			if(!in_array(end(explode('/', $value)), $this->getListLocalFiles($data))){
				copy($this->siteName . $value, $_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'. $this->folder. '/banner/'. $data['code']. '/'. end(explode('/', $value)));
			}			
		}
	}	
	
	/**
	*	Загрузка всех баннеров в диск
	*/
	private function uploadBanners ($data) 
	{
		$currentList = $this->getCurrentJson('list.json');
		
		if(!empty($currentList)){
			// добавление
			foreach ($data['data'] as $value) {
				if(!in_array($value, $currentList[$data['code']]['string'])){

					$end = end(explode('/', $value));			
					$this->upload(
							$data['touch'].'/banner/'.$data['code'].'/'.$end,
							'POST',
							$this->siteName . '/'.$this->serverDiskFolder. '/'. $this->folder.'/banner/'.$data['code'].'/'.$end
						);
				}			
			}			
			// удаление			
			foreach ($currentList[$data['code']]['string'] as $val)	{
				if(!in_array($val, $data['data'])){

					$end = end(explode('/', $val));
					if(!empty($end)){
						$this->upload(
							$data['touch'].'/banner/'.$data['code'].'/'.$end,
							'DELETE'
						);
					}
				}
			}		
		}		
	}
	
	/**
	*	Создаем строку плейлиста баннеров
	*/
	private function makeArray ($data) 
	{
		foreach ($data['data'] as $value) {
			if(end(explode('.', $value)) == 'mp4') {
				$newArray[] = "{'src':['banner/".$data['code']."/".end(explode('/', $value))."']}";
			} else {
				$newArray[] = "{'src':['fake.webm'],'isSlide':true,'poster':'banner/".$data['code']."/".end(explode('/', $value))."'}";
			}
		}

		return implode(',', $newArray);
	}
	
	/**
	*	Создаем строку js файла
	*
	*/
	private function getStringBannersPlaylist()	
	{
		return 
			$this->firstPart.
			$this->makeArray($this->smallBanners).
			$this->centerPart.
			$this->makeArray($this->bigBanners).
			$this->endPart;
	}
	
	/**
	*	Перезаписываем строку js файла на ftp
	*/
	private function overwriting ()	
	{
		if(file_put_contents($_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/'.$this->jsFile['name'], $this->getStringBannersPlaylist()) != false){
			return true;
		}
		return false;
	}
	
	/**
	*	Перезаписываем js файл на диске
	*/
	private function uploadPlaylist () 
	{
		if(	$this->overwriting() &&
			$this->uploadOverwrite($this->jsFile['touch'].'/'.$this->jsFile['name'], 'GET', $_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/'.$this->jsFile['name']) ){
			return true;
		}
		return false;			
	}
	
	/**
	*	Создаем json файл. Сохраняем на сервер.
	*/
	private function setListJson ()	
	{
		$exitArray = array(
			'small' => array(
				'date' => date("Y-m-d H:i:s"),
				'string' => $this->smallBanners['data']
			),
			'big' => array (				
				'date' => date("Y-m-d H:i:s"),
				'string' => $this->bigBanners['data']
			)
		);
		
		$exitString = json_encode($exitArray, JSON_UNESCAPED_UNICODE);
		
		file_put_contents($_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/list.json', $exitString);
		
		return $exitArray;
	}
	
	/**
	*	Отправляем json файл на диск
	*/
	private function uploadJson () 
	{
		$this->setListJson();
		
		if($this->uploadOverwrite($this->jsonFile['touch'].'/'.$this->jsonFile['name'], 'GET', $_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/list.json')){
			return true;
		}
		return false;
	}
	
	/**
	*	Создаем index файл. Сохраняем на сервер.
	*/
	private function setIndexFile () 
	{
		$needString = file_get_contents($this->siteName. '/'.$this->serverDiskFolder. '/'.$this->folder.'/index.php');
		if(file_put_contents($_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/index.html', $needString)){
			return true;
		}
		return false;
	}
	
	/**
	*	Выгружаем index файл в диск
	*/
	public function uploadIndexFile () 
	{
		$this->setIndexFile();
		
		if (filesize($_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/index.html') > 100000 && 
			$this->uploadOverwrite($this->folder.'/index.html', 'GET', $_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/index.html')) 
		{
			return true;
		}
		return false;
	}
	
	/**
	*	Получаем текущий json список
	*/
	private function getCurrentJson ($path)	
	{
		$yandexPath = "https://cloud-api.yandex.net/v1/disk/resources?path=".$this->folder."/".$path;

		$response = $this->apiRequest($yandexPath, "GET", true);
		if ($response) {
			return json_decode(file_get_contents(json_decode($response, true)['file']), true);	
		}
		return "";	
	}
	
	/**
	*	Получаем список всех картинок
	*/
	private function getImages () 
	{	
		$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_157");

		$arFilter = Array("IBLOCK_ID"=>IntVal($this->iBlockIdShop), "ACTIVE"=>"Y");
		
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5000), $arSelect);
		
		while($ob = $res->GetNextElement())	{
			$arFields = $ob->GetFields();
			
			$arr_all[] = Array(
				"ID" => $arFields["ID"],
				"NAME" => $arFields["NAME"],
				"PREVIEW_PICTURE" => $arFields["PREVIEW_PICTURE"],
				"MORE_PHOTO" => $arFields["PROPERTY_157_VALUE"]
			);	

		}
		$this->imageFromDB = $arr_all;
	}
	
	/**
	*	Создаем json файл картинок. Выгружаем в диск
	*/
	public function uploadListImagesJson ()	
	{
		foreach($this->imageFromDB as $value) {
			if(!empty($value['PREVIEW_PICTURE'])){
				$listLogo[] = CFile::GetPath($value['PREVIEW_PICTURE']);
			}	

			if(!empty($value['MORE_PHOTO'])){
				$listDetail[] = CFile::GetPath(array_shift($value['MORE_PHOTO']));
			}			
		}		
		
		$exitArray = array(
			'logo' => array(
				'date' => date("Y-m-d H:i:s"),
				'string' => $listLogo
			),
			'detailfotoshop' => array (				
				'date' => date("Y-m-d H:i:s"),
				'string' => $listDetail
			)
		);
		
		$exitString = json_encode($exitArray, JSON_UNESCAPED_UNICODE);
		
		file_put_contents($_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/listImages.json', $exitString);
		
		if($this->uploadOverwrite($this->folder.'/listImages.json', 'GET', $_SERVER["DOCUMENT_ROOT"]. '/'.$this->serverDiskFolder. '/'.$this->folder.'/listImages.json')){
			return true;
		}
		return false;
	}

	/**
	 * Метод загрузки картинок в диск
	 */
	private function uploadImage ($type) 
	{
		$currentList = $this->getCurrentJson('listImages.json');
		
		if(!empty($currentList)){
			foreach ($this->imageFromDB as $data)
			{
				$imageArray = ($type == "logo") ? $data['PREVIEW_PICTURE'] : array_shift($data['MORE_PHOTO']);

				if(!empty($imageArray)){
					
					$path = CFile::GetPath($imageArray);

					if(!in_array($path, $currentList[$type]['string'])){
						$this->upload(
							$this->folder.'/data/'.$type.'/'.end(explode('/', $path)),
							'POST',
							$this->siteName.$path
						);						
					}					
				}				
			}			
		}
	}
	
	/**
	*	Выгружаем Лого в диск
	*/
	private function uploadLogo () 
	{
		$this->uploadImage("logo");
	}
	
	/**
	*	Выгружаем детальные картинки в диск
	*/
	public function uploadDetailImage () 
	{
		$this->uploadImage("detailfotoshop");
	}
	
	/**
	*	Выгужаем данные в диск
	*/
	public function uploadShopData () 
	{
		// забираем данные из битрикс
		$this->addData();

		if($this->uploadIndexFile()){
			$this->uploadLogo();
			$this->uploadDetailImage();
			$this->uploadListImagesJson();
		}	
	}
	
	/**
	*	Выгужаем данные в диск
	*
	*/
	public function uploadReclama () 
	{
		// забираем данные из битрикс
		$this->addData();

		$this->copyBanners($this->smallBanners);
		$this->copyBanners($this->bigBanners);
		$this->uploadBanners($this->smallBanners);
		$this->uploadBanners($this->bigBanners);		
		$this->uploadPlaylist();
		$this->uploadJson();		
	}
	
	/**
	 * Части js кода требуемого для создания плейлиста
	 */
	private $firstPart = "function syncPlayList(options) {
    'use strict';

    var player = this;

    checkOptions(options);
    var serverTimeField = options.serverTimeFieldName || 'serverTime';
    var videosField = options.videosFieldName || 'videos';
    return init(options);

    function init(options) {
        var serverTime;
        var id  = options.playListId;
        return http(id).then(function(res) {			
            res = JSON.parse(res);
            var videos = extractVideos(res, options);
            player.playList(videos, {
                getVideoSource: options.getVideoSource && options.getVideoSource.bind(player)
            });
            return player;
        });
    }

    function checkOptions(options) {
        if (!options.playListId && options.playListId !== 0) {
            throw new Error('options.playListId required');
        }
    }

    function extractVideos(res, options) {
        var videos = res[videosField];
        if (options.rowVideosList) videos = JSON.parse(videos);
        return videos;
    }

	function http(play_id) {
        return new Promise(function(resolve, reject) {
			var video;
			
			if (play_id == 'bottom') {
				video = [";
				
	private $centerPart = "];
				} else {
					video = [";
					
	private $endPart = "];
				}
					
				var video_all = {'data' : JSON.stringify(video)};
				
				resolve(JSON.stringify(video_all));
			});
		}
	}
	videojs.plugin('syncPlayList', syncPlayList);";		
}